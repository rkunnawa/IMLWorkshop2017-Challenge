import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
sns.set()  # cosmetics
from sklearn import metrics
import itertools
import os


def get_standard():
    df = None
    if os.path.exists('train.csv'):
        df = pd.read_csv('train.csv')
    else:
        df = pd.read_csv('/eos/user/k/kreczko/SWAN_projects/IMLWorkshop2017-Challenge/Results/train.csv')
    return df.drop('Unnamed: 0', axis=1)


def get_modified():
    df = None
    if os.path.exists('test.csv'):
        df = pd.read_csv('test.csv')
    else:
        df = pd.read_csv('/eos/user/k/kreczko/SWAN_projects/IMLWorkshop2017-Challenge/Results/test.csv')
    return df.drop('Unnamed: 0', axis=1)


def plot(df, variable='mass'):
    plt.hist(df[df['isGluon'] == 0][variable], bins=50,
             alpha=0.5, label='Quarks', normed=True)
    plt.hist(df[df['isGluon'] == 1][variable], bins=50,
             alpha=0.5, label='Gluons', normed=True)
    plt.legend()
    plt.xlabel(variable)


def plotPrediction(X, y, prediction, variable='mass', logy=True, axis=plt):
    axis.hist(X[y == 1][variable], bins=50, alpha=1, label='Gluons (all)')
    axis.hist(X[y == 0][variable], bins=50, alpha=0.5, label='Quarks (all)')
    axis.hist(X[(prediction != y) & (y == 0)][variable],
              bins=50, alpha=1, label='Quarks (misidentified)')
    if logy:
        axis.set_yscale("log")
    axis.set_xlabel(variable)
    axis.legend()


def plot_confusion_matrix(cm, classes,
                          normalize=False,
                          title='Confusion matrix',
                          cmap=plt.cm.Blues, axis=plt):
    """
    This function prints and plots the confusion matrix.
    Normalization can be applied by setting `normalize=True`.
    """
    cax = axis.imshow(cm, interpolation='nearest', cmap=cmap)
    axis.set_title(title)
    tick_marks = np.arange(len(classes))
    axis.set_xticks(tick_marks)
    axis.set_xticklabels(classes, rotation=45)
    axis.set_yticks(tick_marks)
    axis.set_yticklabels(classes)
    plt.colorbar(cax, ax=axis)

    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        print("Normalized confusion matrix")
    else:
        print('Confusion matrix, without normalization')

    print(cm)

    thresh = cm.max() / 2.
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        axis.text(j, i, cm[i, j],
                  horizontalalignment="center",
                  color="white" if cm[i, j] > thresh else "black")

    plt.tight_layout()
    axis.set_ylabel('True label')
    axis.set_xlabel('Predicted label')


def test_method(clf, X_train, X_test, y_train, y_test, X_valid, y_valid, name):
    clf.fit(X_train, y_train)

    expected = y_test
    predicted = clf.predict(X_test)
    print('=' * 15, name, '=' * 15)
    print(metrics.classification_report(expected, predicted))
    confusion_matrix = metrics.confusion_matrix(expected, predicted)
    class_names = ['quark', 'gluon']

    f, ((ax1, ax2), (ax3, ax4), (ax5, ax6)
        ) = plt.subplots(3, 2, figsize=(15, 15))
    plot_confusion_matrix(confusion_matrix, classes=class_names,
                          title='Confusion matrix for {0}, without normalization'.format(
                              name),
                          axis=ax1)
    # Plot normalized confusion matrix
    # plt.figure()
    plot_confusion_matrix(confusion_matrix, classes=class_names, normalize=True,
                          title='Normalized confusion matrix for {0}'.format(
                              name),
                          axis=ax2)

    y_score = clf.predict_proba(X_test)
    plotROC(y_test, y_score, axis=ax3)

    prediction = clf.predict(X_test)
    plotPrediction(X_test, y_test, prediction, axis=ax4)

    y_score = clf.predict_proba(X_valid)
    roc_auc = plotROC(y_valid, y_score, axis=ax5)

    prediction = clf.predict(X_valid)
    plotPrediction(X_valid, y_valid, prediction, axis=ax6)

    plt.show()

    # Save AUC value
    f = open("kreczko_v0_auc.txt", "w")
    f.write(str(roc_auc))
    f.close()
    return clf


def plotROC(y_test, y_score, axis=plt):
    fpr, tpr, thresholds = metrics.roc_curve(y_test, y_score[:, 1])
    roc_auc = metrics.auc(fpr, tpr)
    # axis.figure()
    lw = 2
    axis.plot(fpr, tpr, color='darkorange',
              lw=lw, label='ROC curve (area = %0.2f)' % roc_auc)
    axis.plot([0, 1], [0, 1], color='navy', lw=lw, linestyle='--')
    axis.set_xlim([0.0, 1.0])
    axis.set_ylim([0.0, 1.05])
    axis.set_xlabel('False Positive Rate')
    axis.set_ylabel('True Positive Rate')
    axis.set_title('Receiver operating characteristic')
    axis.legend(loc="lower right")
    return roc_auc
