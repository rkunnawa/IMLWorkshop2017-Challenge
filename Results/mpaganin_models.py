from keras.layers import Input, merge, GRU, Masking, Dropout, Dense, Highway
from keras.models import Model

from keras.layers import Input, merge, GRU, Masking, Dropout, Dense, Highway, LSTM, Bidirectional, Conv1D, Flatten
from keras.models import Model

def fancy_rnn(train_dict):
    # create placeholders
    tracks = Input(shape=train_dict['X_tracks'].shape[1:], name='track_input')
    towers = Input(shape=train_dict['X_towers'].shape[1:], name='towers_input')
    flat = Input(shape=(train_dict['X_flat'].shape[-1], ), name='flat_input')
    inputs = [tracks, towers, flat]

    tracks_masked = Masking(mask_value=-999)(tracks)
    towers_masked = Masking(mask_value=-999)(towers)

    tracks_rnn = Bidirectional(
        LSTM(35, return_sequences=False)
    )(tracks_masked)

    towers_rnn = Bidirectional(
        LSTM(35, return_sequences=False)
    )(towers_masked)

    combined_streams = [towers_rnn, tracks_rnn]
    merged_rnns = merge(combined_streams, mode='mul')

    h = merge([flat, merged_rnns] + combined_streams, mode='concat',
              concat_axis=-1)

    h = Dense(128, activation='relu')(h)

    y = Dense(1, activation='sigmoid')(h)

    return Model([tracks, towers, flat], y)


def fancy_cnn(train_dict):
    # create placeholders
    tracks = Input(shape=train_dict['X_tracks'].shape[1:], name='track_input')
    towers = Input(shape=train_dict['X_towers'].shape[1:], name='towers_input')
    flat = Input(shape=(train_dict['X_flat'].shape[-1], ), name='flat_input')
    inputs = [tracks, towers, flat]

    conv_repr = [
        merge([
            Flatten()(
                Conv1D(40, sz + 1, activation='tanh', border_mode='same')(
                    Conv1D(50, sz, border_mode='same', activation='tanh')(st)
                )
            ) for sz in range(2, 7)
        ], mode='concat')
        for st in [tracks, towers]
    ]

    h = merge([flat] + conv_repr, mode='concat',
              concat_axis=-1)

    h = Dense(128, activation='relu')(h)
    h = Dropout(0.2)(h)

    y = Dense(1, activation='sigmoid')(h)

    return Model([tracks, towers, flat], y)


def legacy_rnn(train_dict):
	# create placeholders
	tracks = Input(shape=train_dict['X_tracks'].shape[1:], name='track_input')
	towers = Input(shape=train_dict['X_towers'].shape[1:], name='towers_input')
	flat = Input(shape=(train_dict['X_flat'].shape[-1], ), name='flat_input')
	inputs = [tracks, towers, flat]

	rnn_inputs = [tracks, towers]
	rnn_specs = [GRU(48), GRU(64)]

	masked_event = [Masking(mask_value=-999)(obj) for obj in rnn_inputs]
        
	y = Dropout(0.2)(merge([
	            rnn(ev) for rnn, ev in zip(rnn_specs, masked_event)
	        ] + [flat], mode='concat'))

	mapping = [Dense(128, activation='relu'), 
	           Dropout(0.2), 
	           Highway(activation='relu'), 
	           Dropout(0.2), 
	           Highway(activation='relu'), 
	           Dropout(0.2),
                   Dense(32, activation='relu'),
                   Dropout(0.1),
	           Dense(1, activation='sigmoid')]

	for L in mapping:
	    y = L(y)

	return Model(input=[tracks, towers, flat], output=y)
