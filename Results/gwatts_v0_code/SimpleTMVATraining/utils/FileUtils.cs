﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleTMVATraining.utils
{
    static class FileUtils
    {
        public class FileLists
        {
            public FileInfo[] _gluon_standard;
            public FileInfo[] _gluon_modified;
            public FileInfo[] _quark_standard;
            public FileInfo[] _quark_modified;
        }


        [Serializable]
        public class DataFilesMissingException : Exception
        {
            public DataFilesMissingException() { }
            public DataFilesMissingException(string message) : base(message) { }
            public DataFilesMissingException(string message, Exception inner) : base(message, inner) { }
            protected DataFilesMissingException(
              System.Runtime.Serialization.SerializationInfo info,
              System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
        }

        /// <summary>
        /// Gets the list of all datafiles.
        /// </summary>
        /// <param name="baseLocation">Where directory where the data is located - as layed out in the challenge</param>
        /// <returns></returns>
        public static FileLists DataFiles(DirectoryInfo baseLocation)
        {
            var r = new FileLists()
            {
                _gluon_modified = new DirectoryInfo(Path.Combine(baseLocation.FullName, "gluons_modified")).EnumerateFiles().ToArray(),
                _gluon_standard = new DirectoryInfo(Path.Combine(baseLocation.FullName, "gluons_standard")).EnumerateFiles().ToArray(),
                _quark_standard = new DirectoryInfo(Path.Combine(baseLocation.FullName, "quarks_standard")).EnumerateFiles().ToArray(),
                _quark_modified = new DirectoryInfo(Path.Combine(baseLocation.FullName, "quarks_modified")).EnumerateFiles().ToArray(),
            };

            if (r._gluon_modified.Length == 0)
            {
                throw new DataFilesMissingException("Unable to find gluons modified");
            }
            if (r._gluon_standard.Length == 0)
            {
                throw new DataFilesMissingException("Unable to fine gluons standard");
            }
            if (r._quark_modified.Length == 0)
            {
                throw new DataFilesMissingException("Unable to find quarks modified");
            }
            if (r._quark_standard.Length == 0)
            {
                throw new DataFilesMissingException("Unable to find quark standard");
            }

            return r;
        }

        /// <summary>
        /// Open a group of files as our typed tree.
        /// </summary>
        /// <param name="files"></param>
        /// <returns></returns>
        public static IQueryable<ChallengeData.treeJets> makeTrainingDataset(FileInfo[] files)
        {
            return ChallengeData.QueryabletreeJets.CreateQueriable(files);
        }
    }
}
